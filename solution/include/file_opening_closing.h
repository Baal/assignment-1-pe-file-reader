#ifndef FILE_OPENING_CLOSING_HEADER
#define FILE_OPENING_CLOSING_HEADER
#include <stdbool.h>
#include <stdio.h>

/// @brief contains file error type information (including FILE_OK providing information about corectness of provided file)
enum file_status {
  FILE_OK = 0,
  FILE_NOT_FOUND,
  FILE_TOO_BIG,
  FILE_IS_CATALOG,
  FILE_NAME_TOO_LONG,
  FILE_TOO_MANY_FILES_IN_SYSTEM,
  FILE_NO_ACCES,
  FILE_ERROR //заменить на прочие при возможности
  //другие ошибки
};

/// @brief contains file or error type informaion
struct file_or_error;

struct file_or_error* file_or_error_open_from_name(const char* name, const char* mod);
bool file_or_error_close_this_file(FILE* file);
bool file_or_error_get_valid(struct file_or_error* f);
FILE* file_or_error_get_value(struct file_or_error* f);
void file_or_error_destroy(struct file_or_error* file);

#endif
