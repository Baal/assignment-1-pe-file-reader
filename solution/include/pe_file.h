#ifndef PE_FILE_HEADER
#define PE_FILE_HEADER

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

struct PEHeader;
struct ImageDataDirectory;
struct PEOptinalHeader;
struct PESectionHeader;

struct Section;

size_t pe_file_section_get_size(const struct Section* section);

struct PEHeader* pe_file_read_header_from_32_version(FILE* in);
struct PESectionHeader* pe_file_find_section_header_by_name_from_32_version(FILE* in, const char* section_name, struct PEHeader* pe_header);
struct Section* pe_file_read_section_by_header(FILE* in, struct PESectionHeader* section_header);
void pe_file_write_section_content_to_bin(FILE* out, const struct Section* section);

void pe_file_section_destroy(struct Section* section);

#endif
