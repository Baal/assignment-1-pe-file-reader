#ifndef FILE_READER_WRITER_HEADER
#define FILE_READER_WRITER_HEADER

#include "pe_file.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

/// @brief contains information of succesefulness of reading from file or information about error type
enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_PIXEL_OUT_OF_RANGE
  /* коды других ошибок  */
};
/// @brief contains information of succesefulness of writing to file or information about error type
enum  write_status  {
  WRITE_OK = 0,
  WRITE_OUT_OF_BONDS,
  WRITE_ERROR,
  WRITE_NOTHING
  /* коды других ошибок  */
};

struct Section* pe_file_read_section_from_file(FILE* in, const char* section, const char* pe_file_version);
void pe_file_write_section_contents_to_file(FILE* out, const struct Section* section, const char* file_format);

#endif
