#include "file_reader_writer.h"
#include "pe_file.h"
#include <errno.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// reading

struct Section* from_version_32(FILE* in, const char* section_name){
    struct PEHeader* peHeader =  pe_file_read_header_from_32_version(in);
    
    struct PESectionHeader* sectionHeader = pe_file_find_section_header_by_name_from_32_version(in, section_name, peHeader);
    struct Section* section = pe_file_read_section_by_header(in, sectionHeader);

    free(peHeader);
    free(sectionHeader);
    return section;
}

// writing

enum write_status to_bin(FILE* out, const struct Section** section){
    const struct Section* _section = *section;

    pe_file_write_section_content_to_bin(out, _section);

    switch(errno){
        case EACCES: {fprintf(stderr, "\nfailed writing image to bmp: EACCES \n"); return WRITE_ERROR;}
        case EFBIG: {fprintf(stderr, "\nfailed writing image to bmp: EFBIG \n"); return WRITE_ERROR;}
        case EISDIR: {fprintf(stderr, "\nfailed writing image to bmp: EISDIR \n"); return WRITE_ERROR;}
        case ENAMETOOLONG: {fprintf(stderr, "\nfailed writing image to bmp: ENAMETOOLONG \n"); return WRITE_ERROR;}
        case ENFILE: {fprintf(stderr, "\nfailed writing image to bmp: ENFILE \n"); return WRITE_ERROR;}
        case ENOENT: {fprintf(stderr, "\nfailed writing image to bmp: ENOENT \n"); return WRITE_ERROR;}
    }

    return WRITE_OK;
}

//interface functions

/// @brief 
struct Section* pe_file_read_section_from_file(FILE* in, const char* section, const char* pe_file_version){
    if (strcmp(pe_file_version, "32") == 0) return from_version_32(in, section);

    fprintf(stderr, "NOT_SUPPORTED_FILE_READING_FORMAT \n");
    return NULL;
}
/// @brief 
/// @param out 
/// @param section 
/// @param file_format 
void pe_file_write_section_contents_to_file(FILE* out, const struct Section* section, const char* file_format){
    if (out == NULL) fprintf(stderr, "cannot_write_to_NULL_file \n");
    enum write_status res = WRITE_NOTHING;
    if (strcmp(file_format, "bin") == 0) res = to_bin(out, &section);

    switch(res){
        case(WRITE_OK): {return;}
        case(WRITE_ERROR): {fprintf(stderr, "/n error during writing to file\n"); return;}
        case(WRITE_OUT_OF_BONDS): {fprintf(stderr, "/n writing out of bonds during writing to file\n"); return;}
        case(WRITE_NOTHING): {fprintf(stderr, "NOT_SUPPORTED_FILE_WRITING_FORMAT \n"); return;}
    }
}
