/// @file 
/// @brief Main application file

#include "file_opening_closing.h"
#include "file_reader_writer.h"
#include "pe_file.h"
#include <stdio.h>
#include <stdlib.h>

/// Application name string
#define APP_NAME "section-extractor"

/// @brief Print usage test
/// @param[in] f File to print to (e.g., stdout)
void usage(FILE *f)
{
  fprintf(f, "Usage: " APP_NAME " <in_file> <section_name> <out_file>\n");
}

/// @brief Application entry point
/// @param[in] argc Number of command line arguments
/// @param[in] argv Command line arguments
/// @return 0 in case of success or error code
int main(int argc, char** argv)
{
  (void) argc; (void) argv; // supress 'unused parameters' warning

  if (argc != 4) {usage(stdout); return -1;}
  
  const char* in_file_name    = argv[1];
  const char* out_file_name   = argv[3];

  const char* section_name    = argv[2];

  struct file_or_error* in_file  = file_or_error_open_from_name(in_file_name, "rb");
  if (file_or_error_get_valid(in_file)  != true) {printf("in_dont_valid");  return -1;}
  struct file_or_error* out_file  = file_or_error_open_from_name(out_file_name, "wb");
  if (file_or_error_get_valid(out_file) != true) {printf("in_dont_valid");  return -1;}

  struct Section* section = pe_file_read_section_from_file(file_or_error_get_value(in_file), section_name, "32");
  pe_file_write_section_contents_to_file(file_or_error_get_value(out_file), section, "bin");

  file_or_error_close_this_file(file_or_error_get_value(in_file));
  file_or_error_close_this_file(file_or_error_get_value(out_file));

  file_or_error_destroy(in_file);
  file_or_error_destroy(out_file);
  pe_file_section_destroy(section);

  return 0;
}
